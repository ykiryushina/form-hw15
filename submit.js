const form = document.getElementById('myForm');
let hiddenInputContainer = document.querySelector('.hiddenInput');


form.addEventListener('submit', (e) => {
    let hiddenInputField = document.getElementById('hidden');
    hiddenInputField ? hiddenInputField.remove() : console.log('No hidden input to be removed');

    function createHiddenInput() {
        if (!document.querySelector('.created-element')) {
            const input = document.createElement('input');
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'newField');
            input.setAttribute('value', 'customValue');
            input.setAttribute('class', 'created-element');
            hiddenInputContainer.append(input);
        } else {
            return;
        }

    }
    createHiddenInput();
    document.querySelector('.form-confirmation').classList.add('_active');
});

